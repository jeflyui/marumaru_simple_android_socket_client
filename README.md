# Simple Android Socket Client

Android app using [Socket.IO](http://socket.io/).
Use the Web application below to run the server.

## Demo
You can test the app [simple_android_socket_client.apk](/uploads/aa5bc933a9cf8ff03c6790c63312475e/simple_android_socket_client.apk)

## Documentation

For more information, see these articles:

- [Simple Web Socket Server](https://gitlab.com/jeflyui/marumaru_simple_web_socket_server/wikis/home)

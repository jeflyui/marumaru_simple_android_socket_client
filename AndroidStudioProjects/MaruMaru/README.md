# Simple Android Socket Client

Android app using [Socket.IO](http://socket.io/).
Use the Web application below to run the server.

## Demo
You can test the app [Here](https://whispering-waters-56400.herokuapp.com/)

## Documentation

For more information, see these articles:

- [Simple Web Socket Server](https://gitlab.com/jeflyui/marumaru_simple_web_socket_server/wikis/home)


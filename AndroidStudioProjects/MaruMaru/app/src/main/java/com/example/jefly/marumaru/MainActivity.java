package com.example.jefly.marumaru;

import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URISyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class MainActivity extends AppCompatActivity {
    private static final String TOKEN = "marumaru2017";
    private Socket socket;
    @BindView(R.id.form_layout) View linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        try {
            socket = IO.socket("https://whispering-waters-56400.herokuapp.com");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        socket.connect();
        socket.on("update", onUpdate);
    }

    private Emitter.Listener onUpdate = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        ((LinearLayout) linearLayout).removeAllViews();
                        JSONArray arrJson = new JSONArray(args[0].toString());
                        for (int i = 0; i < arrJson.length(); i++) {
                            JSONObject jsonObject = arrJson.getJSONObject(i);
                            TextView tv = new TextView(getApplicationContext());
                            tv.setTextColor(Color.BLACK);
                            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                            tv.setText(jsonObject.getString("title"));
                            tv.setLayoutParams(new LinearLayout.LayoutParams(AppBarLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                            EditText et = new EditText(getApplicationContext());
                            et.setTextColor(Color.BLACK);
                            et.setBackgroundColor(Color.parseColor("#e3e4e5"));
                            et.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
                            et.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                            ((LinearLayout) linearLayout).addView(tv);
                            ((LinearLayout) linearLayout).addView(et);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
